# script BASH sass-css

Pour compiler le fichier style.css qui se trouve dans le dossier `assets/scss`, on exécute le script bash `sass-css`.
Pour ce faire : 
* ouvrir votre terminal depuis le dossier racine du projet __Materialize__. 
* Tapez la ligne de commande `bash assets/bash/sass-css`.
Le script exécutera ainsi la commande de compilation du fichier `style.scss`en fichier `style.css`, visible par l'utilisateur. 
Le fichier sera minifié pour une plus grande légèreté de l'exécution de l'application web. 
