// hamberger menu

const root = document.getElementById('container');

const header = document.getElementById('header');
const hambergerMenu = document.getElementById('hamberger_menu');
const closeHeader = document.getElementById('close');
const shadow = document.createElement('div');
shadow.classList.add('active__shadow');

const openMenu = () => {
	header.classList.add('active');
	root.appendChild(shadow);
};
const closeMenu = () => {
	header.classList.remove('active');
	root.removeChild(shadow);
};

// const ariaHidden = (element) => {
//   if
//     (element.getAttribute("aria-hidden", "false")) {
//       element.setAttribute("aria-hidden", "true");
//   } else {
//     element.setAttribute("aria-hidden", "false");
//   }
//   return element;
// };

hambergerMenu.addEventListener('click', openMenu);
closeHeader.addEventListener('click', closeMenu);
shadow.addEventListener('click', closeMenu);

hambergerMenu.addEventListener('keydown', function (e) {
	if (e.key === 'Enter' || e.key === ' ') {
		openMenu();
	}
});
closeHeader.addEventListener('keydown', function (e) {
	if (e.key === 'Enter' || e.key === ' ') {
		closeMenu();
	}
});

// subMenu

// general actions

window.addEventListener('keydown', function (e) {
	if (e.key === 'Escape') {
		closeMenu();
	}
});
